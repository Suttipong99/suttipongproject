package com.yain.suttipong.bigbikereviewandcheckprice;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by User on 8/7/2559.
 */
public class TypebikeDAO {
    private SQLiteDatabase database;
    private Dbhelper dbhelper;

    public TypebikeDAO(Context context) {
        dbhelper = new Dbhelper(context);
        database = dbhelper.getWritableDatabase();
    }

    public void close(){
        database.close();
    }

    public ArrayList<Typebike>fileAll(){
        ArrayList<Typebike> typebikes = new ArrayList<>();
        String sqlText = "SELECT * FROM typebike; ORDER BY typeid DESC;";
        Cursor cursor = database.rawQuery(sqlText, null);
        cursor.moveToFirst();
        Typebike typebike;
        while (!cursor.isAfterLast()){
            typebike = new Typebike();
            typebike.setTypeId(cursor.getString(0));
            typebike.setTypeName(cursor.getString(1));
            typebike.setTypepic(cursor.getString(2));

            typebikes.add(typebike);

            cursor.moveToNext();
        }
        return typebikes;
    }

}
