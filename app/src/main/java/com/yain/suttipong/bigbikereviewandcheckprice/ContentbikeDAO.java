package com.yain.suttipong.bigbikereviewandcheckprice;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by User on 8/7/2559.
 */
public class ContentbikeDAO {
    private SQLiteDatabase database;
    private Dbhelper dbhelper;

    public ContentbikeDAO (Context context){
        dbhelper = new Dbhelper(context);
        database = dbhelper.getWritableDatabase();
    }

    public void close(){
        database.close();
    }
    public ArrayList<Contentbike> fileAll(){
        ArrayList<Contentbike> contentbikes =new ArrayList<>();
        String sqlText = "SELECT * FROM contentbike; ORDER BY bikeid DESC;";
        Cursor cursor = database.rawQuery(sqlText, null);
        cursor.moveToFirst();
        Contentbike contentbike;
        while (!cursor.isAfterLast()){
            contentbike = new Contentbike();
            contentbike.setBikeId(cursor.getString(0));
            contentbike.setBikeName(cursor.getString(1));
            contentbike.setBikePic(cursor.getString(2));
            contentbike.setBikeData(cursor.getString(3));
            contentbike.setBikePrice(cursor.getString(4));
            contentbike.setTypeId(cursor.getString(5));

            contentbikes.add(contentbike);

            cursor.moveToNext();
        }
        return contentbikes;
    }
}
