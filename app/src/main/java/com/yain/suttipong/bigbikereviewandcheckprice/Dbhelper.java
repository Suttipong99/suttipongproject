package com.yain.suttipong.bigbikereviewandcheckprice;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by User on 8/7/2559.
 */
public class Dbhelper extends SQLiteOpenHelper {
    private static final String databaseName = "bigbike.sqlite";
    private static final int databaseVersion = 1;
    private Context myContext;

    public Dbhelper(Context context) {
        super(context, databaseName, null, databaseVersion);
        this.myContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String SQLText = "CREATE TABLE typebike (" +
                "typeid TEXT PRIMARY KEY," +
                "typename TEXT," +
                "typepic TEXT);";
        db.execSQL(SQLText);

        SQLText = "CREATE TABLE contentbike (" +
                "bikeid TEXT PRIMARY KEY," +
                "bikename TEXT," +
                "bikepic TEXT," +
                "bikedata TEXT," +
                "bikeprice TEXT," +
                "typeid TEXT);";
        db.execSQL(SQLText);


        SQLText = "INSERT INTO typebike (typeid, typename, typepic)"+
                "VALUES ('001', 'ER6N', 'er');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO typebike (typeid, typename, typepic)"+
                "VALUES ('002', 'Z800', 'z8');";
        db.execSQL(SQLText);

        SQLText = "INSERT INTO contentbike (bikeid, bikename, bikepic, bikedata, bikeprice, typeid)"+
                "VALUES ('B001', 'ER6N', 'er', 'รุ่นนี้เเรงมาก', '200000', '001');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO contentbike (bikeid, bikename, bikepic, bikedata, bikeprice, typeid)"+
                "VALUES ('B002', 'Z800', 'Z8', 'รุ่นนี้เเรงมากๆ', '200000', '002');";
        db.execSQL(SQLText);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
