package com.yain.suttipong.bigbikereviewandcheckprice;

import java.io.Serializable;


public class Contentbike implements Serializable {
    private String bikeId;
    private String bikeName;
    private String bikePic;
    private String bikeData;
    private String bikePrice;
    private String typeId;

    public String getBikeId() {
        return bikeId;
    }

    public void setBikeId(String bikeId) {
        this.bikeId = bikeId;
    }

    public String getBikeName() {
        return bikeName;
    }

    public void setBikeName(String bikeName) {
        this.bikeName = bikeName;
    }

    public String getBikePic() {
        return bikePic;
    }

    public void setBikePic(String bikePic) {
        this.bikePic = bikePic;
    }

    public String getBikeData() {
        return bikeData;
    }

    public void setBikeData(String bikeData) {
        this.bikeData = bikeData;
    }

    public String getBikePrice() {
        return bikePrice;
    }

    public void setBikePrice(String bikePrice) {
        this.bikePrice = bikePrice;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
}
