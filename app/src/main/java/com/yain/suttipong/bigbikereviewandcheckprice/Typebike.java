package com.yain.suttipong.bigbikereviewandcheckprice;

import java.io.Serializable;

public class Typebike implements Serializable{
    private String typeId;
    private String typeName;
    private String typepic;

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTypepic() {
        return typepic;
    }

    public void setTypepic(String typepic) {
        this.typepic = typepic;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}


