package com.yain.suttipong.bigbikereviewandcheckprice;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class TypebikeListAdapter extends BaseAdapter {

    private static Activity activity;
    private static LayoutInflater layoutInflater;
    private ArrayList<Typebike> typebikes;

    public TypebikeListAdapter(Activity activity, ArrayList<Typebike> typebikes) {
        this.activity = activity;
        this.typebikes = typebikes;
        this.layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return typebikes.size();
    }

    @Override
    public Typebike getItem(int position) {
        return this.typebikes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(this.typebikes.get(position).getTypeId());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        view = layoutInflater.inflate(R.layout.listview_type,null);
        ImageView img = (ImageView)view.findViewById(R.id.img_type);
        TextView tv =(TextView)view.findViewById(R.id.text_typename);

        tv.setText(this.typebikes.get(position).getTypeName());

        return view;
    }
}
