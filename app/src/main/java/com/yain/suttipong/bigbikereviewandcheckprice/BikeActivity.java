package com.yain.suttipong.bigbikereviewandcheckprice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class BikeActivity extends AppCompatActivity {

    private TextView name, data, price;
    private ImageView img;
    private Typebike typebike;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bike);

        Intent intent = getIntent();
        typebike = (Typebike) intent.getSerializableExtra("bikeObj");

        name = (TextView) findViewById(R.id.text_bikename);
        img = (ImageView) findViewById(R.id.img_bike);
        data = (TextView) findViewById(R.id.text_bikedata);
        price = (TextView) findViewById(R.id.text_bikeprice);

        name.setText(typebike.getTypeName());


    }
}
